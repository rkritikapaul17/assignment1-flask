import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = True
    CSRF_ENABLED = True
    SECRET_KEY = 'qwertyuiop'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URI']

class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DB_URI = 'postgresql://postgres:1234@127.0.0.1/Users'
    DB_CONNECTION_STRING = os.environ.get('DB_CONNECTION_STRING', os.environ.get('SQLALCHEMY_DATABASE_URI', DB_URI))
    DEVELOPMENT = True
    DEBUG = True